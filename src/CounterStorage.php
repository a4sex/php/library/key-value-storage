<?php

namespace A4Sex;

trait CounterStorage
{
    const COUNTER_PREFIX = 'counter_';

    public function counterKey($key): string
    {
        return self::COUNTER_PREFIX . $key;
    }

    public function increment($key, $expires = null): void
    {
        $counter = $this->counterKey($key);
        $value = $this->load($counter, 0) + 1;
        $this->save($counter, $value, $expires);
    }

    public function decrement($key, $expires = null): void
    {
        $counter = $this->counterKey($key);
        $value = $this->load($counter, 0) - 1;
        $this->save($counter, $value, $expires);
    }

    public function count($key)
    {
        return $this->load($this->counterKey($key), 0);
    }

    public function resetCounter($key): void
    {
        $this->delete($this->counterKey($key));
    }

}
