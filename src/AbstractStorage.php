<?php

namespace A4Sex;

use Psr\Cache\CacheItemPoolInterface;
use Psr\Cache\CacheItemInterface;

class AbstractStorage
{
    protected CacheItemPoolInterface $cache;

    public function __construct(CacheItemPoolInterface $memory)
    {
        $this->cache = $memory;
    }

    public function clear($key = null): void
    {
        if ($key) {
            throw new \InvalidArgumentException('Call with $key is deprecated');
        }
        $this->cache->clear();
    }

    public function item($key): CacheItemInterface
    {
        return $this->cache->getItem($key);
    }

    public function touch($key, $expires = null): void
    {
        $value = $this->load($key);
        $this->save($key, $value, $expires);
    }

    public function load($key, $default = null)
    {
        return $this->item($key)->get() ?? $default;
    }

    public function point($expires): ?\DateTime
    {
        if ($expires) {
            return new \DateTime('+'.$expires.' seconds');
        }
        return null;
    }

    protected function saveItem($item, \DateTimeInterface $expires = null): void
    {
        if ($expires) {
            $item->expiresAt($expires);
        }
        $this->cache->save($item);
    }

    public function delete($key): void
    {
        $this->cache->deleteItem($key);
    }

}
